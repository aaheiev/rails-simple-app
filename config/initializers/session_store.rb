Rails.application.config.session_store :redis_session_store, {
    key: '_rails_simple_app',
    redis: {
        expire_after: 90.minutes,  # cookie expiration
        ttl:          90.minutes,  # Redis expiration, defaults to 'expire_after'
        key_prefix:   "rails-simple-app:session:",
        url:          "redis://#{ENV['REDIS_SERVER_ADDRESS']}:6379/#{ENV['REDIS_SESSION_DB']}",
    }
}
